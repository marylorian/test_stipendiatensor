var timer = null;
//функция, ограничивающая рандомное число значением max c округлением до целого
//max - максимальное значение числа
//min - минимальное значение числа
//returns целое число
function rand(min, max){
	if (max){
		return Math.floor(Math.random()*(max-min+1))+min;
	}
	else{
		return Math.floor(Math.random()*(min+1));
	}
}

//функция, создающая массив рандомных чисел длины 10
//max - максимальное значение числа
//min - минимальное значение числа
//length - длина массива(по умолчанию 10)
//returns массив рандомных чисел
function random_array(length = 10){
	var length = parseInt(length);
	var min = 0;
	var max = 255;
	var arr = new Array(length);
	for (var i=0; i<length; i++){
		arr[i] = rand(min, max);
	}
	return arr;
}

//функция, вызываемая по клику на Random, печать массива рандомных чисел в html
//array - массив рандомных чисел 
//html_array - массив в виде строки для вывода на html
//returns void
function print_random_array(){
	stop_sort();
	var array = random_array();
	var html_array = array[0];
	for(var i=1; i<10; i++){
		html_array += "&nbsp&nbsp" + array[i];
	}
	$('#array').html(html_array);
}


//функция, вызываемая по клику на Sorting, выводит массив и вызывает сортировку 
//returns void
$(document).ready(function() {
	$('#sort_button').click(function() {
		if(print_for_sort()==0)
			return;
		timer = setTimeout(bubble, 1000, 0, 0);
	});
});


//функция, парсит и выводит на печать сгенерированный ранее массив чисел 
//в виде отдельных элементов
//string - строка со сгенерированным рандомным массивом 
//array - массив рандомных чисел 
//html_array - массив в виде строки для вывода на html в виде отдельных элементов
//returns 0 - в случае, когда Random нажата раньше Sorting
function print_for_sort(){
	var string = $('#array').html();
	var array = string.split('&nbsp;&nbsp;');
	var html_array = '<p class="arr_elem" id = arr0>'+array[0]+'</p>';
	if(array[1]==undefined){
		$('#sorting_array').html("Сначала необходимо нажать на Random");
		return 0;
	}
	for(var i = 1; i<10; i++){
		html_array += '<p class="arr_elem" id = arr'+i+'>'+array[i]+'</p>';
	}
	$('#sorting_array').html(html_array);
}


//производит сортировку пузырьком, подсвечивая активные элементы
//iter - номер итерации
//elem - номер элемента в массиве
//objId1, objId2 - id элементов на html 
//first - первый элемент из сравниваемой пары чисел
//second - второй элемент из сравниваемой пары чисел
//returns void
function bubble(iter, elem){
	$('#sort_button').prop("disabled",true);
	$('#iter').html("Проход "+(iter+1));
	if (elem == 9){
		if (iter == 10) {
			$("#sort_button").prop("disabled",false);
			return;
		}
		iter++;
		elem = 0;
	}
	i = elem;
	elem++;

	$('#iter').html("Проход "+(iter+1));

	objId1 = '#arr'+i;
	first = parseInt($(objId1).html());
	set_blue(objId1);

	objId2 = '#arr'+(i+1);
	second = parseInt($(objId2).html());
	set_blue(objId2);

	if(first>second){
		set_red(objId1);
		change(objId1, objId2);
	}
	setTimeout(set_white, 500, objId1, objId2);
	timer = setTimeout(bubble, 1000, iter, elem);
}

//функция, получает на вход id двух элементов, анимированно меняет местами элементы
//objId1, objId2 - id элементов на html 
//returns void
function change(objId1, objId2){
	$(objId1).animate({opacity: 0.1}, "fast");
	$(objId2).animate({opacity: 0.1}, "fast");

	$(objId1).html(second);
	$(objId2).html(first);

	set_red(objId2);

	$(objId1).animate({opacity: 1}, "fast");
	$(objId2).animate({opacity: 1}, "fast");
}

//функция, получает на вход id двух элементов, делает их фон белым, а шрифт черным
//objId1, objId2 - id элементов на html 
//returns void
function set_white(objId1, objId2){
	$(objId1).css({
		color:'black',
		padding:'5px',
		background:'white'
	});
	$(objId2).css({
		color:'black',
		padding:'5px',
		background:'white'
	});
}

//функция, получает на вход id элемента, делает фон синим, а шрифт белым
//objId- id элемента на html 
//returns void
function set_blue(objId){
	$(objId).css({
		color:'#ffffff',
		padding:'5px',
		background:'blue'
	});
}

//функция, получает на вход id элемента, делает фон красным, а шрифт белым
//objId- id элемента на html 
//returns void
function set_red(objId){
	$(objId).css({
		color:'#ffffff',
		padding:'5px',
		background:'red'
	});
}

//функция, прерывает асинхронно выполняющиеся функции, включает кнопку сортировки
//returns void
function stop_sort(){
	if (timer) {
        clearTimeout(timer);
        timer = null;
        $("#sort_button").prop("disabled",false);
    }
}